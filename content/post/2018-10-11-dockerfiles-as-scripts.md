---
title: Dockerfiles as scripts
subtitle: Guaranteeing consistency
date: 2018-10-11
tags: ["docker", "dockerfile", "scripting"]
---

Despite the big push towards containers, many (most?) applications are still 
running on servers or virtual machines. Said applications might have a neat and 
clean build script with little dependencies, but if they do, I never 
encountered one.

<!--more-->

More often the build script was written at the project start by people who 
have long left the company and touched up over the years to keep up with 
small changes in the requirements: the habitat of these scripts is an 
environment that evolved over the years, existing in a delicate balance where 
any change will doom the build process.

### Just use Docker
The first reaction to these sort of problems is to replicate the environment in 
a docker container and have the script run in it.

While this is a viable solution, it has the drawback of introducing the 
complexity of creating, versioning, storing, retrieving and using the correct 
image for the script. Despite the fact that an infrastructure might be
already in place (an automation server to build and version the image, a Docker 
registry to store it, an orchestration solution to handle the containers, etc), 
there is a simpler answer.

### The solution
The solution I adopted is to perform the steps necessary to reproduce the 
script output directly during the `docker build` by using a Dockerfile: no need 
to save and handle the resulting image since the Dockerfile itself accomplishes 
the task.

As a practical example here is the simple build of a project which requires a 
specific node version and extra php libraries. The Dockerfile leverages the 
power of [multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/) 
to accomplish the task.

<center>**Dockerfile**</center>
{{< highlight Dockerfile "linenos=table" >}}
FROM node:6 AS frontend
RUN mkdir app
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

COPY src src
RUN npm run build:prod

FROM composer
RUN apk add --update libpng-dev \
    && docker-php-ext-install gd \
    && mkdir /app
WORKDIR /app
COPY composer.json composer.lock ./
RUN composer install

COPY src src
COPY --from=frontend /app ./
RUN cd / && tar czf app.tar.gz \
    --exclude='/app/build/node_modules' /app
{{< / highlight >}}

If we have a Jenkins server we can write a simple Jenkinsfile to run our
Dockerfile and store the resulting artifact:

<center>**Jenkinsfile**</center>
{{< highlight groovy "linenos=table" >}}
node('mars-jenkins-slave-docker') {
  BRANCH = params.BRANCH

  stage('Build') {
    checkout scm
    ansiColor('xterm') {
      def customImage = docker.build("proj-builder", "-f ops/build.Dockerfile .")
      customImage.inside('-v $WORKSPACE:/output') {
        sh "cp /app.tar.gz /output/${BRANCH}.tar.gz"
      }
    }
  }

  archiveArtifacts "${BRANCH}.tar.gz"
}
{{< / highlight >}}

The artifact can be retrieved later for deploying

### Advantages
There are several advantages on having the Dockerfile and Jenkinsfile:

- the Dockerfile can be tested on any machine, without the need to commit and
  push the changes to check if they work
- the Dockerfile is a widespread standard and is pretty straightforward to read 
  and understand making it a good solution for a standard for building
  applications
- the declarative nature of both files combined with the decoupling they 
  provide from the rest of the infrastructure ensure consistency across 
  executions
- both files reside in the project repository: the developers can take full 
  ownership of the build process
- by residing in the project repository  each branch can have a different build 
  process tailored for the needs of the feature being developed

These advantages come without the need for additional infrastructure needed to 
handle images and containers

### Pitfall
Caching is a powerful feature of Docker: in the above Dockerfile we leverage it 
twice:

1. In the frontend we first copy the `package.json` and `package-lock.json` and 
   run the `npm install`, only after we copy the source code and run 
   `npm run build:prod` 
2. Likewise in the backend we copy `composer.json` and `composer.lock`, run the
   install and then copy the rest of the code

This way run the `(npm|composer) install` only when the respective json/lock 
file is changed, busting the cache

This behaviour is useful enough to warrant a mention in the 
[Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache), 
but what if we are creating a Dockerfile to replace a script? Surely we want it 
to run every time we run  `docker build` regardless if there were any change or 
not.

To get around this problem we have two options:

#### 1. Use the argument `--no-cache=true` when running the build:
<center>**Jenkinsfile**</center>
{{< highlight groovy  >}}
def customImage = docker.build(
  "script-runner", 
  "-f scripts/script.Dockerfile --no-cache=true ."
)
{{< / highlight >}}
This way we force all the steps in the Dockerfile to run, but also _force all 
the steps in the Dockerfile to run_, meaning we risk adding a lot of overhead 
by setting up the whole environment every time the script needs to run.

#### 2. Bust the cache manually:
<center>**Jenkinsfile**</center>
{{< highlight groovy  >}}
def customImage = docker.build("script-runner", "-f scripts/script.Dockerfile .")
sh "echo ${BUILD_NUMBER} > buster.txt"
{{< / highlight >}}
<center>**Dockerfile**</center>
{{< highlight groovy  >}}
COPY buster.txt ./
RUN script.sh
{{< / highlight >}}
With this approach we have the flexibility to chose exactly which steps will be 
repeated and which ones will be cached by copting a file that is guaranteed to 
be different from last run

### Conclusions
The Dockerfile thus provides us with a good solution when we need to gurantee 
consistency or simply we don't want to take care of maintaining a dedicated 
environment for a script to run in.

We also can leverage the power of `docker build` to avoid having to adopt the 
whole container ecosystem to run scripts.

Furthermore it can be the first step in dockerizing an application: it is 
fairly easy to define the runtime environment by adding a further step to the 
Dockerfile and generate an image to be used by an orchestrator.

