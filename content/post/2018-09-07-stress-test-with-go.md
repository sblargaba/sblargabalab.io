---
title: Stress test with go
subtitle: Introducing a pointless http server
date: 2018-09-07
tags: ["go", "http", "logging", "stress test"]
---

Here is a simple, CPU intensive http server with horribly verbose logging 
that performs a pointless task.

<!--more-->

The reason for this marvel of code is for me to have something to deploy, 
monitor, log and do unpleasant generic Operations _things_ to.

### Impact with golang
My only real programming experience have been with Python, so the only logical 
choice for me was to write the application in [go](https://golang.org/) in 
order to learn something new, and because it's what all the cool kids are doing.

I have to admint I was pleasantly suprised by the experience: while the syntax 
is undoubtly more complicated than Python (ok fine, I admit it, I have a crush 
on Python) the documentation is easy to follow and there are great resources 
online.

Apart from Stackoverflow, the places I ~~copy and pasted~~ took inspiration 
from are: 

- [Writing Web Applications](https://golang.org/doc/articles/wiki/) - from the 
main golang website, a great introduction on how to use the built in http
server to... well, serve requests
- [Go by Example](https://gobyexample.com/) - a lifesaver reference on how to 
implement a lot of simple things

At the moment of writing the [application code](https://gitlab.com/sblargaba/go-http-test/blob/292a6658675a599652d2af1f604c633bfcd67b8f/http.go) 
is a ~130 lines file, so I don't assume I'm an expert, but I have to say 
it was a fun experiment

### The problem
The main advantage of golang is how horribly fast it is: I could not simply 
serve a blank page since that wouldn't generate much load on whatever will be 
running the code: I needed to write some pointless and poorly optimized 
algorithm: cue in [Project Euler](https://projecteuler.net/).

> Project Euler exists to encourage, challenge, and develop the skills and 
> enjoyment of anyone with an interest in the fascinating world of mathematics.  
> --- Project Euler main page

Project Euler provides a database of problems that require some algorithm 
writing skills to solve.  
I randomly choosed [Problem #7](https://projecteuler.net/problem=7) 
from the archives as a challenge

>By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, 
>we can see that the 6th prime is 13.  
>What is the 10 001st prime number?  
>--- Project Euler Problem #7


### The solution
What I have is a simple function that checks if a number is multiple of any of 
the elements of an ordered slice of primes

{{< highlight go "linenos=table,linenostart=18" >}}
func isMultiple(number uint64, primes []uint64) bool {
	for _, prime := range primes {
		log.Debug().Msgf("Testing %d on %d", prime, number)
		if prime*prime > number {
			// number is not multiple of any of the primes in the array
			return false
		}
		if number%prime == 0 {
			// number is multiple of a prime
			return true
		}
	}
	return false
}
{{< / highlight >}}

In order to generate more load and output, I inserted a line in the loop to
print a debug message: this drammatically slows down the execution and will
give me something to play with in the future when I'll dive in log management

### How to use it
As explained in the [README](https://gitlab.com/sblargaba/go-http-test/blob/292a6658675a599652d2af1f604c633bfcd67b8f/README.md), 
the application provides a http server on port 8080 and will calculate the nth 
prime number, with `N` provided on the URL: for example 
`http://localhost:8080/100` will return the 100th prime number (541, apparently)

The URL can also be used to set the log level in order to speed up execution or 
just avoid printing thousands of log lines (e.g.
`http://localhost:8080/info/10000`)

Having the algorithm accessible with parameters from a URL will allow me to 
deploy the application in a container and stress test it with a http benchmark 
tool

### Everything else
I tried to provided everything else a well mannered application should have:

- A simple [Dockerfile](https://gitlab.com/sblargaba/go-http-test/blob/292a6658675a599652d2af1f604c633bfcd67b8f/Dockerfile) 
(following the [multi-stage](https://docs.docker.com/develop/develop-images/multistage-build/) pattern)
- A simple [Makefile](https://gitlab.com/sblargaba/go-http-test/blob/292a6658675a599652d2af1f604c633bfcd67b8f/Makefile)
- Some [tests](https://gitlab.com/sblargaba/go-http-test/blob/292a6658675a599652d2af1f604c633bfcd67b8f/http_test.go)

The last thing missing would be providing a metrics endpoint for monitoring, 
but I will add that when I'll start playing with [Prometheus](https://prometheus.io/) 
or a similar tool

