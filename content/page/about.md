---
title: About me
subtitle: whoami
comments: false
---

My name is **Davide** and I do Operations stuff for money; some people  would 
go as far as to say I am a DevOps Engineer, but many foam at the mouth at the 
very concept

In my free time I also play with Operation stuff, but they told me there's no 
money in _that_; in these pages I document some of those explorations in my 
playground

This website is built with [Hugo](https://gohugo.io/) and is 
[licensed](https://gitlab.com/sblargaba/sblargaba.gitlab.io/blob/master/LICENSE)
under _Creative Commons Attribution 4.0 International_

